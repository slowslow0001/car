#!/usr/bin/python
# -*- coding: utf-8 -*-
import random
import time
import os
import numpy as np 
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, FormatStrFormatter


# 讓右轉所產生的障礙數據變的更加真實
def Turn_Right_very_real():
    global car_angle
    while True:
        r = random.randint(0,1)
        if r == 0:
            print "NO_EXIT_BOX"
            print "Car State:Front"
            print "Angle:"+str(car_angle)
            x.append(x[-1])
            y.append(y[-1]-1)
        elif r == 1:
            print "EXIT_BOX"
            car_angle = 0 #先左轉90度回去車頭朝前方在直走 
            print "Car State:Turn_Left 90º"
            print "Car State:Front"
            print "Angle:"+str(car_angle)
            x.append(x[-1]+1)
            y.append(y[-1])
            break

# 讓左轉所產生的障礙數據變的更加真實
def Turn_Left_very_real():
    global car_angle
    while True:
        r = random.randint(0,1)
        if r == 0:
            print "NO_EXIT_BOX"
            print "Car State:Front"
            x.append(x[-1])
            y.append(y[-1]+1)
        elif r == 1:
            print "EXIT_BOX"
            car_angle = 0 #先右轉90度回去車頭朝前方在直走 
            print "Car State:Turn_Right 90º"
            print "Car State:Front"
            x.append(x[-1]+1)
            y.append(y[-1])
            break




def Single_Box():
    global car_angle
    if (hinder["Left"] < 30.0) & (hinder["Right"] > 30.0) & (hinder["Front"] > 30.0):
        print "Car State:Front"
        print "Angle:"+str(car_angle) 
        x.append(x[-1]+1)
        y.append(y[-1])

    elif (hinder["Left"] > 30.0) & (hinder["Right"] < 30.0) & (hinder["Front"] > 30.0):
        print "Car State:Front"
        print "Angle:"+str(car_angle) 
        x.append(x[-1]+1)
        y.append(y[-1])

    elif (hinder["Left"] > 30.0) & (hinder["Right"] > 30.0) & (hinder["Front"] < 30.0):
        if hinder["Right"]>hinder["Left"]:
            car_angle += 90
            print "Car State:Turn_Right 90º" # 車子右轉90度
            print "Car State:Front"
            print "Angle:"+str(car_angle) # 車頭方向
            x.append(x[-1])#轉向後再直走所以x軸不會變ㄝ,改變的為y軸
            y.append(y[-1]-1)
            Turn_Right_very_real()

        elif hinder["Left"]>hinder["Right"]:
            car_angle -= 90
            print "Car State:Turn_Left 90º"
            print "Car State:Front"
            print "Angle:"+str(car_angle) 
            x.append(x[-1])
            y.append(y[-1]+1)
            Turn_Left_very_real()




def Double_Box():
    global car_angle
    if (hinder["Left"] < 30.0) & (hinder["Right"] < 30.0) & (hinder["Front"] > 30.0):
        print "Car State:Front"
        print "Angle:"+str(car_angle) 
        x.append(x[-1]+1)
        y.append(y[-1])

    elif (hinder["Left"] < 30.0) & (hinder["Right"] > 30.0) & (hinder["Front"] < 30.0):
        car_angle += 90
        print "Car State:Turn_Right 90º" # 車子右轉90度
        print "Car State:Front"
        print "Angle:"+str(car_angle) # 車頭方向
        x.append(x[-1])#轉向後再直走所以x軸不會變ㄝ,改變的為y軸
        y.append(y[-1]-1)
        Turn_Right_very_real()
    elif (hinder["Left"] > 30.0) & (hinder["Right"] < 30.0) & (hinder["Front"] < 30.0):
        car_angle -= 90
        print "Car State:Turn_Left 90º"
        print "Car State:Front"
        print "Angle:"+str(car_angle) 
        x.append(x[-1])
        y.append(y[-1]+1)
        Turn_Left_very_real()


def Triple_Box():
    global car_angle
    if (hinder["Left"] > 30.0) & (hinder["Right"] > 30.0) & (hinder["Front"] > 30.0):
        print "Car State:Front"
        print "Angle:"+str(car_angle) 
        x.append(x[-1]+1)
        y.append(y[-1])
    elif (hinder["Left"] < 30.0) & (hinder["Right"] < 30.0) & (hinder["Front"] < 30.0):
        pass # 先不考慮三邊都有障礙物



def Car_Control():
    Single_Box()
    Double_Box()
    Triple_Box()


def Senser_Val():
    s = 0
    Start = time.time()   # Car 一開始假設為Front
    while s < 100:
        # 每3s 產生一筆障礙物數據  Car 根據數據進行路線修改（車頭轉向）
        # 假設在dleay 3s 之前 車子是持續Front 此時沒有障礙物(車子在座標平面上移動一格)
        print "car State:Front"
        x.append(x[-1]+1)
        y.append(y[-1])
        time.sleep(0.1)
        for w in range(3):
            if w == 0:
                hinder["Front"] = round(random.uniform(10, 100), 2)
            elif w == 1:
                hinder["Right"] = round(random.uniform(10, 100), 2)
            elif w == 2:
                hinder["Left"] = round(random.uniform(10, 100), 2)
        print "<<hinder>>" + "Front:" + str(hinder["Front"]) + " " + "Right:"\
            + str(hinder["Right"]) + " " + "Left:" + str(hinder["Left"])
        Car_Control()
        s += 1
        End = time.time()
    Total = End - Start
    Car_Distance = Total * 10
    print "Car-Time:" + str(Total)
    print "Car_Distance:" + str(Car_Distance)


def main():
    try:
        Senser_Val()
    except KeyboardInterrupt:
        pass
    finally:
        npx = np.array(x)
        npy = np.array(y)
        print "x" + str(npx)
        print "y" + str(npy)
        plt.figure()
        plt.plot(npx,npy)
        plt.yticks(np.linspace(-15, 15, 28))

        # plt.xticks(npx, np.linspace(-10,10,1), rotation=0)  
        # plt.yticks(npy, np.linspace(-10,10,1), rotation=0)  
        plt.grid()
        
        plt.show()


# 車子 1s 前進 10cm


hinder = {"Front": 0.0, "Right": 0.0, "Left": 0.0} #障礙物的位置 

car_angle = 0 #車子目前的角度

x = [0]
y = [0]

if __name__ == "__main__":
    main()
